(function () {
    'use strict';

    angular.module('bowlingApp', [
        'app.core',
        'bowlingApp.bowling'
    ]);

})();
