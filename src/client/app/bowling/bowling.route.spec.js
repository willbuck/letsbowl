/* jshint -W117, -W030 */
describe('bowling', function() {
    describe('states', function() {
        var controller;
        var views = {
            bowling: 'app/bowling/bowling.html'
        };

        beforeEach(function() {
            module('bowlingApp.bowling', bard.fakeToastr);
            bard.inject('$location', '$rootScope', '$state', '$templateCache');
            $templateCache.put(views.core, '');
        });

        it('should map / route to ', function() {
            expect($state.get('bowling').templateUrl).to.equal(views.bowling);
        });

        it('of bowling module should work with $state.go', function() {
            $state.go('bowling');
            $rootScope.$apply();
            expect($state.is('bowling'));
        });
    });
});
