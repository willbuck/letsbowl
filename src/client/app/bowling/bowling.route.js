(function() {
    'use strict';

    angular
        .module('bowlingApp.bowling')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper) {
        var otherwise = '/404';
        routerHelper.configureStates(getStates(), otherwise);
    }

    function getStates() {
        return [
            {
                state: 'bowling',
                config: {
                    url: '/',
                    templateUrl: 'app/bowling/bowling.html',
                    controller: 'BowlingController',
                    controllerAs: 'bowling',
                    title: 'Lets Bowl!'
                }
            }
        ];
    }
})();
