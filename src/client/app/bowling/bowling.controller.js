(function () {
    'use strict';

    angular
        .module('bowlingApp.bowling')
        .controller('BowlingController', BowlingController);

    BowlingController.$inject = ['BowlingService'];
    /* @ngInject */
    function BowlingController(BowlingService) {
        var vm = this;
        // What does this controller need to do?
        var currentPlayer = 0;
        var currentFrame = 0;
        vm.players = [];
        vm.playersEntered = false;
        vm.addBall = addBall;
        vm.addPlayer = addPlayer;

        init();

        function addBall(ball) {
            var thisPlayer = vm.players[currentPlayer];
            thisPlayer.balls.push(ball);
            var thisFrame = thisPlayer.frames[currentFrame];
            thisFrame.balls[thisFrame.balls.length] = ball;
            incrementPlayer(thisFrame, thisPlayer);
        }

        function incrementPlayer(thisFrame, thisPlayer) {
            if(thisFrame.balls.length == 2) {
                scoreTotal(thisFrame, thisPlayer);
                if(vm.players.length == (currentPlayer + 1)) {
                    currentPlayer = 0;
                    currentFrame++;
                } else {
                    currentPlayer++;
                }
            }
        }

        function scoreTotal(thisFrame, thisPlayer) {
            // TODO need promises here
            thisFrame.total = BowlingService.scoreBowling(thisPlayer.balls, currentFrame);
        }

        function addPlayer(name) {
            name = name || ('Unknown Bowler'+ getRandomInt(1,10));
            vm.players.push(createPlayer(name));
            vm.newPlayer = '';
        }

        function createFrames() {
            var frame = {balls: []};
            var frames = [];
            for(var i = 0; i < 10; i++) {
                frames.push(angular.copy(frame));
            }
            return frames;
        }

        function createPlayer(name) {
            var player = {name: name, frames: createFrames(), balls: []};
            return player;
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }

        function init() {
            addPlayer('The Dude');
            addPlayer('Ernie McCracke');
        }
    }
})();
