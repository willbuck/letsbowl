/* jshint -W117, -W030 */
describe('BowlingController', function() {
    var controller;

    beforeEach(function() {
        bard.appModule('bowlingApp.bowling');
        bard.inject('$controller');
    });

    beforeEach(function () {
        controller = $controller('BowlingController');
    });

    it('should be created successfully', function () {
        expect(controller).to.be.defined;
    });

    describe('init', function() {
        it('should initialize the first player to be The Dude', function() {
            expect(controller.players[0]).to.be.defined;
            expect(controller.players[0].frames.length).to.equal(10);
            expect(controller.players[0].name).to.equal('The Dude');
        });
        it('should initialize the second player to be Ernie McCracken', function() {
            expect(controller.players[0]).to.be.defined;
            expect(controller.players[0].frames.length).to.equal(10);
            expect(controller.players[0].name).to.equal('The Dude');
        });
    });

    describe('addBall', function() {
        it('should take a ball and add it to the current players current frame', function() {
            controller.addBall(4);
        });
    });

    describe('addPlayer', function() {
        it('should take a name and push a new object into players', function() {
            var newPlayerName = 'Roy';
            expect(controller.players.length).to.equal(2);
            controller.addPlayer(newPlayerName);
            expect(controller.players.length).to.equal(3);
            expect(controller.players[2].name).to.equal(newPlayerName);
            expect(controller.players[2].frames.length).to.equal(10);
        });
    });
});
