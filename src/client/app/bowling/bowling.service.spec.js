/* jshint -W117, -W030 */
describe('BowlingService', function() {
    var service;
    var partialGame = [3,4,5,1,0,9,6];
    var partialGameWithSparesAndStrikes = [10,5,5,10,10,8,1]; // 5 frames complete
    // (10 + 5 + 5) + (5 + 5 + 10) + (10 + 10 + 8) + (10 + 8 + 1) + (8 + 1) = 96

    var fullGame = [8,0,7,2,6,2,9,0,5,1,0,6,2,7,7,0,9,0,3,2];
    // 8 + 9 + 8 + 9 + 6 + 6 + 9 + 7 + 9 + 5 = 76

    var fullGameWithSparesAndStrikes = [6,4,10,10,10,8,1,7,0,9,1,10,9,1,0,4];
    // (6 + 4 + 10 = 20) + (10 + 10 + 10 = 30) + (10 + 10 + 8 = 28) +
    // (10 + 8 + 1 = 19) + (8 + 1) + (7 + 0) + (9 + 1 + 10 = 20) +
    // (10 + 9 + 1 = 20) + (9 + 1 + 0 = 10) + (0 + 4) = 167

    var perfectGame = [10,10,10,10,10,10,10,10,10,10,10,10]; // 300
    var nearlyPerfectGame = [10,10,10,10,10,10,10,10,10,9,1,10]; // 1 + 10 + 10 less than perfect


    beforeEach(function() {
        bard.appModule('bowlingApp.bowling');
        bard.inject('BowlingService');
    });

    beforeEach(function () {
        service = BowlingService;
    });

    it('should be created successfully', function () {
        expect(service).to.be.defined;
    });

    describe('scoreBowling', function() {
        it('should return 0 if given an empty array of ballsBowled', function () {
            var results = service.scoreBowling([], 1);
            expect(results).to.equal(0);
        });

        it('should be able to properly score a partial game', function () {
            var results = service.scoreBowling(partialGame, 4); // 3.5 frames, 28 total
            expect(results).to.equal(28);
        });

        it('should be able to properly score a partial game with strikes and spares', function () {
            var results = service.scoreBowling(partialGameWithSparesAndStrikes, 5);
            expect(results).to.equal(96);
        });

        it('should be able to properly score a full game with no strikes or spares', function () {
            var results = service.scoreBowling(fullGame);
            expect(results).to.equal(76);
        });

        it('should be able to give the correct score for a stopAtFrame before the end of ballsBowled', function () {
            var resultsAtFrame3 = service.scoreBowling(fullGame, 3);
            // 8 + 9 + 8 = 25
            expect(resultsAtFrame3).to.equal(25);

            var resultsAtFrame5 = service.scoreBowling(fullGame, 5);
            // 8 + 9 + 8 + 9 + 6 = 40
            expect(resultsAtFrame5).to.equal(40);

            var resultsAtFrame6 = service.scoreBowling(fullGame, 6);
            // 8 + 9 + 8 + 9 + 6 + 6 = 46
            expect(resultsAtFrame6).to.equal(46);
        });

        it('should be able to properly score a full game with various strikes and spares', function () {
            var results = service.scoreBowling(fullGameWithSparesAndStrikes);
            expect(results).to.equal(167);
        });

        it('should be able to properly score perfect game', function () {
            var results = service.scoreBowling(perfectGame);
            expect(results).to.equal(300);
        });

        it('should be able to properly score spares in the 10th frame', function () {
            var results = service.scoreBowling(nearlyPerfectGame);
            expect(results).to.equal(279);
        });

    });

});
