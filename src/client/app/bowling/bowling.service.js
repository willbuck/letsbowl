(function () {
    'use strict';

    angular
        .module('bowlingApp.bowling')
        .factory('BowlingService', BowlingService);

    BowlingService.$inject = [];
    /* @ngInject */
    function BowlingService() {
        var service = {
            scoreBowling: scoreBowling
        };

        function scoreBowling(ballsBowled, stopAtFrame) {
            stopAtFrame = stopAtFrame || 10;
            return scoreBowlingRecursive(ballsBowled.slice(), 1, stopAtFrame);
        }

        // Mutates the array, complex
        function scoreBowlingRecursive(ballsBowled, frame, stopAtFrame) {
            var sum;
            // Base case to handle unstarted game
            if(ballsBowled.length === 0) {
                sum = 0;
            } else if(frame === stopAtFrame) {
                if(ballsBowled.length === 3) {
                    sum = ballsBowled[0] + ballsBowled[1] + ballsBowled[2];
                } else if(ballsBowled.length >= 2) {
                    sum = ballsBowled[0] + ballsBowled[1];
                } else {
                    sum = ballsBowled[0];
                }
            } else {
                var thisFrame = ballsBowled.shift();
                if(thisFrame === 10) { // Strike
                    sum = thisFrame + ballsBowled[0] + ballsBowled[1] +
                        scoreBowlingRecursive(ballsBowled, frame+1, stopAtFrame);
                } else if(ballsBowled.length === 0) {
                    sum = thisFrame;
                } else {
                    thisFrame += ballsBowled.shift();
                    if(thisFrame === 10) { // Spare
                        sum = thisFrame + ballsBowled[0] +
                            scoreBowlingRecursive(ballsBowled, frame+1, stopAtFrame);
                    } else {
                        sum = thisFrame + scoreBowlingRecursive(ballsBowled, frame+1, stopAtFrame);
                    }
                }
            }
            return sum;
        }

        return service;
    }
})();
